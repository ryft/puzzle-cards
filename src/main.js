import App from './App.svelte';

// Primitive routing based on GET params
let params = (new URL(document.location)).searchParams;

const pages = ['featured', 'create', 'puzzle', 'edit', 'about'];

let page = 'featured';
for (let p of pages) {
    if (params.has(p)) {
        page = p;
        break;
    }
}

const app = new App({
    target: document.body,
    props: {
        page: page,
        puzzle_id: params.get('id'),
        puzzle_pw: params.get('pw'),
    },
});
