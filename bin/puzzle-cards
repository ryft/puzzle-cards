#!/usr/bin/env perl

use Mojolicious::Lite;
use Mojolicious::Plugin::Redis;

use Bytes::Random::Secure qw(random_string_from);
use JSON::XS;
use Readonly;
use Time::HiRes qw(gettimeofday);
use Try::Tiny;

# Automatically expire puzzles after a week
Readonly my $PUZZLE_TTL => 60 * 60 * 24 * 7;

# Max length of received puzzles
Readonly my $MAX_PUZZLE_LENGTH => 1024;

# Max key collision avoidance attempts
Readonly my $MAX_KEY_ATTEMPTS => 5;

# Puzzle password parameters
Readonly my $PW_LENGTH => 8;
Readonly my $PW_ALPHABET => join('', 'a'..'z', 'A'..'Z', 0..9);
Readonly my $PW_REGEX => qr/^[$PW_ALPHABET]{$PW_LENGTH}$/;

# Pattern to match reserved puzzle keys
Readonly my $RESERVED_REGEX => qr/^featured-/i;

plugin 'redis', { 
    server => 'localhost:6379',
    helper => 'redis',
    debug => 0,
};

helper is_valid_password => sub {
    my ($c, $pw) = @_;
    return 1 if $pw and $pw =~ m/$PW_REGEX/;
};

get '/api/get/:id' => sub {
    my $c = shift;

    my $puzzle = $c->redis->get($c->param('id'))
        or return $c->reply->not_found;

    my $decoded = try { decode_json($puzzle) }
        or return $c->reply->exception('Invalid puzzle');

    # Don't return the password unless it matches the provided one
    if ($decoded->{password}) {
        delete $decoded->{password} if !$c->param('pw')
            or $decoded->{password} ne $c->param('pw');
    }

    # Refresh expiry when fetching non-reserved puzzles
    $c->redis->expire($c->param('id'), $PUZZLE_TTL)
        unless ($c->param('id') =~ m/$RESERVED_REGEX/);

    $c->render(json => $decoded);
};

post '/api/set' => sub {
    my $c = shift;

    my $json = $c->req->json;
    my $puzzle = $json->{puzzle}
        or return $c->render(json => { error => 'No puzzle provided' });

    # Reject puzzles that are too large
    return $c->render(json => { error => 'Puzzle too large' })
        if length($puzzle) > $MAX_PUZZLE_LENGTH;

    # Test whether the puzzle is valid
    return $c->render(json => { error => 'Invalid puzzle provided' })
        unless $puzzle->{version} and $puzzle->{version} =~ /^\d$/;

    my $key = $json->{key};
    if ($key) {
        # Validate user-supplied key
        $key =~ s/[^\w-]//g;
        if (length $key < 5 || length $key > 20) {
            return $c->render(json => { error => 'Invalid puzzle name' });
        }
        if ($key =~ m/$RESERVED_REGEX/) {
            return $c->render(json => { error => 'Puzzle name cannot start with "featured-"' });
        }
        if ($c->redis->exists($key)) {
            my $puzzle = decode_json($c->redis->get($key));
            if (my $pw = $json->{password}) {
                return $c->render(json => { error => 'Incorrect puzzle password' })
                    unless $puzzle->{password} and $pw and $puzzle->{password} eq $pw;
            } else {
                return $c->render(json => { error => 'Puzzle name already used' });
            }
        }

    } else {
        # Key is simply the current milliseconds from current time
        # Try a few times to avoid race conditions before failing
        my $attempts = 0;
        while (!$key && $attempts++ < $MAX_KEY_ATTEMPTS) {
            my (undef, $micro) = gettimeofday();
            my $candidate = sprintf("%06d", $micro);
            $key = $candidate unless $c->redis->exists($candidate);
        }
        return $c->render(json => { error => 'Puzzle name exhaustion' }) unless $key;
    }

    # Generate a password for editing/removing this puzzle if not reserved
    $puzzle->{password} = random_string_from($PW_ALPHABET, $PW_LENGTH)
        unless $c->is_valid_password($puzzle->{password})
            or $key =~ m/$RESERVED_REGEX/;

    if ($c->redis->set($key, encode_json($puzzle))) {
        $c->redis->expire($key, $PUZZLE_TTL);
        $c->render(json => { key => "$key", password => "$puzzle->{password}" });
    }
};

post '/api/completed' => sub {
    my $c = shift;

    my $json = $c->req->json;
    my $key = $json->{key}
        or return $c->render(json => { error => 'No puzzle key provided' });

    my $puzzle = decode_json($c->redis->get($key))
        or return $c->render(json => { error => 'Unknown puzzle key' });

    $puzzle->{completions}++;
    $c->redis->set($key, encode_json($puzzle));
    $c->render(json => { key => "$key", completions => $puzzle->{completions} });
};

get '/api/featured' => sub {
    my $c = shift;

    # Packaged Redis lib doesn't appear to support SCAN
    my @keys = sort { $b cmp $a } $c->redis->keys('featured-*');

    my @puzzles = ();
    for my $key (@keys) {
        try {
            my $puzzle_json = $c->redis->get($key);
            push @puzzles, decode_json($puzzle_json);
        };
    }

    $c->render(json => \@puzzles);
};

# Remove static paths, nginx does that for us
app->static->paths([]);

app->start;
